package com.fixtureforce;

import java.io.IOException;

import com.fixtureforce.app.*;
import com.oleh.FirstName;
import com.oleh.LastName;
import com.oleh.MiddleName;

public class Main {
    public static String firstname;
    public static String lastname;
    public static String middlename;
    public static FirstName fn = new FirstName();
    public static LastName ln = new LastName();
    public static MiddleName mn = new MiddleName();

    public static void main(String[] args) throws IOException {
        // Step 1
        Generator generator = new Generator();
        GenEntity ent = generator.gen("{\"name\": \"first_name\", \"type\": \"integer\"}");

        String f1 = fn.firstN(firstname);
        String l1 = ln.lastN(lastname);
        String m1 = mn.middleN(middlename);

        System.out.println(l1+" "+f1+" "+m1);

        //System.out.println(ent.getValue());


        // Step 2
        // TODO Implement interface
        // GenFactory factory = new GenFactory();
        // factory.setCount(20);
        // factory.setLang("UA");
        // factory.add(generator.init("{\"name\": \"first_name\", \"type\": \"name\"}"));
        // String out = factory.generateAsJsonString();

        /*
        #out:
        [
            {
                "first_name": "Иван"
            },
            {
                "first_name": "Николай"
            },
        ]
         */
    }
}
