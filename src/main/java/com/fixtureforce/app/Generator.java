package com.fixtureforce.app;

import com.google.gson.Gson;

public class Generator {
    
    private String config;
    private GenEntity genEntity;

    public Generator() {
    }

    public Generator init(String config) {
        this.config = config;

        Gson g = new Gson();
        this.genEntity = g.fromJson(config, GenEntity.class);

        return this;
    }

    public GenEntity gen(String config) {
        this.init(config);
        return this.generate();
    }

    private GenEntity generate() {
        // TODO Add this.genEntity validation

        String type = this.genEntity.getType();

        if (type.equals("integer")) {
            // TODO Generate rand int
            // Use for it class IntegerFixture

            Integer i = 1234;
            this.genEntity.setIntValue(i);
        } else if (type.equals("name")) {
            // TODO Generate rand name
            // Use for it class NameFixture

            String str = "test";
            this.genEntity.setStringValue(str);
        } else {
            // TODO Add error handler
        }

        return this.genEntity;
    }
}
