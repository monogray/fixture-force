package com.oleh;

import com.fixtureforce.Main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FirstName extends Main{

    private static final String FILENAME = "firstname.txt";

    public static String firstN(String firstname) throws IOException {

        BufferedReader br;
        FileReader fr;

        try {
            fr = new FileReader(FILENAME);
            br = new BufferedReader(fr);

            String sCurrentLine;

            br = new BufferedReader(new FileReader(FILENAME));
            List listA = new ArrayList();
            int i = 0;

            while ((sCurrentLine = br.readLine()) != null) {
                listA.add(i, sCurrentLine);
                i = i + 1;
            }

            int size = listA.size();
            int f = (int) Math.floor(Math.random() * size);

            firstname = (String) listA.get(f);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return firstname;
    }
}