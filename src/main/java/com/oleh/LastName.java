package com.oleh;

import com.fixtureforce.Main;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class LastName extends Main
{
    private static final String FILENAME = "lastname.txt";

    public static String lastN(String lastname) throws IOException {

        BufferedReader br;
        FileReader fr;

        try {
            fr = new FileReader(FILENAME);
            br = new BufferedReader(fr);

            String sCurrentLine;

            br = new BufferedReader(new FileReader(FILENAME));
            List listA = new ArrayList();
            int i = 0;

            while ((sCurrentLine = br.readLine()) != null) {
                listA.add(i, sCurrentLine);
                i = i + 1;
            }

            int size = listA.size();
            int f = (int) Math.floor(Math.random() * size);

            lastname = (String) listA.get(f);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lastname;
    }
}
