package com.oleh;

import com.fixtureforce.Main;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class MiddleName extends Main
{
    private static final String FILENAME = "middlename.txt";

    public static String middleN(String middlename) throws IOException {

        BufferedReader br;
        FileReader fr;

        try {
            fr = new FileReader(FILENAME);
            br = new BufferedReader(fr);

            String sCurrentLine;

            br = new BufferedReader(new FileReader(FILENAME));
            List listA = new ArrayList();
            int i = 0;

            while ((sCurrentLine = br.readLine()) != null) {
                listA.add(i, sCurrentLine);
                i = i + 1;
            }

            int size = listA.size();
            int f = (int) Math.floor(Math.random() * size);

            middlename = (String) listA.get(f);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return middlename;
    }
}
